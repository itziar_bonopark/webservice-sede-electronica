<?php
include('lib/nusoap.php');
include('funciones_server.php');
header('Content-Type: text/html; charset=UTF-8');

$server = new soap_server();
$server->configureWSDL("bicis", "urn:bicis");
    
$server->register("getAccesoTramites",
	array("identificacion" => "xsd:string", "pin" => "xsd:string"),
	array("return" => "xsd:string"),
	"urn:bicis",
	"urn:bicis#getAccesoTramites",
	"rpc",
	"encoded",
	"Caso de uso encargado de dar acceso a los tramites de la bici");
	
$server->register("getAltaAbonado",
	array(	"tipo_documento" => "xsd:string", 
			"identificacion" => "xsd:string",
			"nombre" => "xsd:string",
			"apellido1" => "xsd:string",
			"apellido2" => "xsd:string",
			"fecha_nacimiento" => "xsd:string",
			"telefono" => "xsd:string",
			"email" => "xsd:string",
			"direccion" => "xsd:string",
			"municipio" => "xsd:string",
			"provincia" => "xsd:string",
			"codigo_postal" => "xsd:string",
			"abono_transporte" => "xsd:string",
			"saldo_inicial" => "xsd:string"),
	array("return" => "xsd:string"),
	"urn:bicis",
	"urn:bicis#getAltaAbonado",
	"rpc",
	"encoded",
	"Caso de uso encargado de realizar un alta de una autorizacion para un abonado");
        
$server->register("getModificarAbonado",
array(	"identificacion" => "xsd:string",
		"nombre" => "xsd:string",
		"apellido1" => "xsd:string",
		"apellido2" => "xsd:string",
		"fecha_nacimiento" => "xsd:string",
		"telefono" => "xsd:string",
		"email" => "xsd:string",
		"direccion" => "xsd:string",
		"municipio" => "xsd:string",
		"provincia" => "xsd:string",
		"codigo_postal" => "xsd:string"),
array("return" => "xsd:string"),
"urn:bicis",
"urn:bicis#getModificarAbonado",
"rpc",
"encoded",
"Caso de uso encargado de realizar una modificacion de una autorizacion para un abonado");

$server->register("getBajaAbonado",
array(	"identificacion" => "xsd:string"),
array("return" => "xsd:string"),
"urn:bicis",
"urn:bicis#getBajaAbonado",
"rpc",
"encoded",
"Caso de uso encargado de realizar la baja de una autorizacion para un abonado");

$server->register("getRecargaSaldo",
array(	"identificacion" => "xsd:string"),
array("return" => "xsd:string"),
"urn:bicis",
"urn:bicis#getRecargaSaldo",
"rpc",
"encoded",
"Caso de uso encargado de realizar la recarga de saldo de la tarjeta para las bicis");

$server->register("getConsultaSaldo",
array(	"identificacion" => "xsd:string"),
array("return" => "xsd:string"),
"urn:bicis",
"urn:bicis#getConsultaSaldo",
"rpc",
"encoded",
"Caso de uso encargado de realizar la consulta de saldo de la tarjeta para las bicis");

$server->register("getDuplicadoTarjeta",
array(	"identificacion" => "xsd:string"),
array("return" => "xsd:string"),
"urn:bicis",
"urn:bicis#getDuplicadoTarjeta",
"rpc",
"encoded",
"Caso de uso encargado de realizar un duplicado de tarjeta de un abonado");

		
$server->service($HTTP_RAW_POST_DATA);
?>
