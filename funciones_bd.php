<?php
define('DB_USER', "ip");
define('DB_PASSWORD', "CuIgDijamweedMouf4");
define('DB_DATABASE', "bpkio");
define('DB_SERVER', "themis1a.mad.bpk.io");
define('DB_PORT',"5432");
define('search_path',"neo_kr3");

///Funcion para Abrir la Base de Datos///
function abrir_bd()
{
	$dbconn =  pg_connect("host=".DB_SERVER." port=".DB_PORT." dbname=".DB_DATABASE." user=".DB_USER." password=".DB_PASSWORD) ;  
	if($dbconn)
	{
		pg_query($dbconn, "set search_path =".search_path);
	}
	else
	{
		error_log('No se ha podido conectar: ' . pg_last_error($dbconn)); 
	}

	return $dbconn;

}

///Funcion para Cerrar la Base de Datos///
function cerrar_bd($dbconn)
{
	pg_close($dbconn);
}

function selectInfoUsuario($dni, $password,$dbconn) 
{
	$query = 'select * from neo_kr3.f_get_user_by_dni_auth($1,$2);';
	
	$result = pg_query_params($dbconn, $query, array($dni, $password)) or die('La consulta fallo: ' . pg_last_error());
				
	if(!$result)
	{
		error_log('La consulta fallo: ' . pg_last_error($dbconn).'la sentencia es: '.$query);
	}
	else
	{
		if ($row = pg_fetch_array($result, null, PGSQL_ASSOC)) 
		{
			$response=1;
		}
	}
	pg_free_result($result);

	return $response;
}

function can_subscribe($dni,$dbconn)
{
	$query = 'select * from neo_kr3.f_can_subscribe_by_dni($1) AS resultado;';
	
	$result = pg_query_params($dbconn, $query, array($dni)) or die('La consulta fallo: ' . pg_last_error());
				
	if(!$result)
	{
		error_log('La consulta fallo: ' . pg_last_error($dbconn).'la sentencia es: '.$query);
	}
	else
	{
		if ($row = pg_fetch_array($result, null, PGSQL_ASSOC)) 
		{
			$response=$row['resultado'];
		}
	}
	pg_free_result($result);

	return $response;
}

function updateUsuario($dni,$nombre,$apellido1,$apellido2,$fecha_nacimiento,$telefono,$email,$direccion,$municipio,$provincia,$codigo_postal,$dbconn)
{
		$query = 'select * from neo_kr3.f_update_resident($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11)';
			
		$result = pg_query_params($dbconn, $query, array($nombre, $apellido1, $apellido2, $email, $direccion, $municipio, $provincia, $codigo_postal, $telefono, $fecha_nacimiento,$dni)) or die('La consulta fallo: ' . pg_last_error());
	
		if ($result) 
		{
			$response= 1;
		} 
		else 
		{
			$response = 0;
		}
	      	
		pg_free_result($result);
    	return $response;
}

function cancell_subscription($dni,$dbconn)
{
	$query = 'select * from neo_kr3.f_update_user_activate($1,$2,$3)';
			
	$result = pg_query_params($dbconn, $query, array($dni, 3, 0)) or die('La consulta fallo: ' . pg_last_error());

	if ($result) 
	{
		$query2 = 'select * from neo_kr3.f_cancell_subscription_user($1,$2)';
		$result2 = pg_query_params($dbconn, $query2, array($dni, 5)) or die('La consulta fallo: ' . pg_last_error());
		if($result2)
		{
			$response=1;
		}
		else
		{
			$response=0;
		}
	}
	else
	{
		$response=0;
	}
	pg_free_result($result);
	pg_free_result($result2);
	return $response;
}

function check_active_subscrition($dni,$dbconn)
{
	$sentencia="select * from neo_kr3.f_get_active_subscription_by_dni($1) AS activado";
	$result = pg_query_params($dbconn,$sentencia,array($dni)) ; 
	if(!$result)
	{
		error_log('La consulta fallo: ' . pg_last_error($dbconn).'la sentencia es: '.$sentencia);
	}
	else
	{
		if ($row = pg_fetch_array($result, null, PGSQL_ASSOC)) 
		{
			$activado=$row['activado'];
		}
	}
	pg_free_result($result);
	return $activado;
}

function check_amount_user($dni,$dbconn)
{
	$sentencia="select * from neo_kr3.f_get_activate_subscription($1)";
	$result = pg_query_params($dbconn,$sentencia,array($dni)) ; 
	if(!$result)
	{
		error_log('La consulta fallo: ' . pg_last_error($dbconn).'la sentencia es: '.$sentencia);
	}
	else
	{
		if ($row = pg_fetch_array($result, null, PGSQL_ASSOC)) 
		{
			$saldo=$row['amount'];
		}
	}
	pg_free_result($result);
	return $saldo;
}

function duplicate_card($dni,$motivo,$dbconn)
{
	$sentencia_usuario="select * from neo_kr3.f_get_activate_subscription($1)";
	$result_usuario = pg_query_params($dbconn,$sentencia_usuario,array($dni)); 
	if(!$result_usuario)
	{
		error_log('La consulta fallo: ' . pg_last_error($dbconn).'la sentencia es: '.$sentencia_usuario);
	}
	else
	{
		if ($row_usuario = pg_fetch_array($result_usuario, null, PGSQL_ASSOC))
		{
			$saldo=$row_usuario['amount'];
			$clave=$row_usuario['pickup_key'];
			
		}
	}
	
	
	$ok=true;
	pg_query("BEGIN"); // Inicio de Transacción
	$sentencia_abono="select * from neo_kr3.f_cancell_subscription_user($1,$2)";
	$result_abono = pg_query_params($dbconn,$sentencia_abono,array($dni,$motivo));
	if(!$result_abono)
	{
		$ok=false;	
		pg_query("ROLLBACK");
		error_log('La consulta fallo: ' . pg_last_error($dbconn).'la sentencia es: '.$sentencia_abono);
	}
	else
	{
		$sentencia_actualiza_usuario="select * from neo_kr3.f_duplicate_user_subscription($1,$2,$3)";
		$result_actualiza_usuario = pg_query_params($dbconn,$sentencia_actualiza_usuario,array($dni,2,$saldo));
		if(!$result_actualiza_usuario)
		{
			$ok=false;	
			pg_query("ROLLBACK");
			error_log('La consulta fallo: ' . pg_last_error($dbconn).'la sentencia es: '.$sentencia_actualiza_usuario);
		}
	}
	if($ok)
	{
		pg_query("COMMIT");
	}
	
	pg_free_result($result_usuario);
	pg_free_result($result_abono);
	pg_free_result($result_actualiza_usuario);
	
	$resultado=$ok."/".$clave;
	return $resultado;
}

?>
