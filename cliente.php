<html>
<head>
<title>Title</title>
<meta charset="utf-8"/>
</head>
<body>

<?php
include('lib/nusoap.php');

$cliente = new nusoap_client("http://localhost/WS/soap_server.php?wsdl", true);
      
    $error = $cliente->getError();
    if ($error) {
        echo "<h2>Constructor error</h2><pre>" . $error . "</pre>";
    }
      
    $result = $cliente->call("getAccesoTramites", array("identificacion" => "33333333P","pin" => "miguel25"));
      
    if ($cliente->fault) {
        echo "<h2>Fault</h2><pre>";
        print_r($result);
        echo "</pre>";
    }
    else {
        $error = $cliente->getError();
        if ($error) {
            echo "<h2>Error</h2><pre>" . $error . "</pre>";
        }
        else {
            echo "<h2>Acceso Trámites Bici</h2><pre>";
            echo $result;
            echo "</pre>";
        }
    }
    
    
    $result = $cliente->call("getAltaAbonado", 
    array(	"tipo_documento" => "1", 
			"identificacion" => "72494410C",
			"nombre" => "nombre",
			"apellido1" => "apellido1",
			"apellido2" => "apellido2",
			"fecha_nacimiento" => "07-06-1985",
			"telefono" => "123456789",
			"email" => "i.perez@booster-bikes.com",
			"direccion" => "direccion",
			"municipio" => "municipio",
			"provincia" => "provincia",
			"codigo_postal" => "31006",
			"abono_transporte" => "NO",
			"saldo_inicial" => "10"));
      
    if ($cliente->fault) {
        echo "<h2>Fault</h2><pre>";
        print_r($result);
        echo "</pre>";
    }
    else {
        $error = $cliente->getError();
        if ($error) {
            echo "<h2>Error</h2><pre>" . $error . "</pre>";
        }
        else {
            echo "<h2>Alta autorización abonado</h2><pre>";
            echo $result;
            echo "</pre>";
        }
    }
    
    $result = $cliente->call("getModificarAbonado", 
    array(	"identificacion" => "78746570K",
			"nombre" => "itziar",
			"apellido1" => "apellido1",
			"apellido2" => "apellido2",
			"fecha_nacimiento" => "07-06-1985",
			"telefono" => "123456789",
			"email" => "i.perez@booster-bikes.com",
			"direccion" => "direccion",
			"municipio" => "municipio",
			"provincia" => "provincia",
			"codigo_postal" => "31"));
      
    if ($cliente->fault) {
        echo "<h2>Fault</h2><pre>";
        print_r($result);
        echo "</pre>";
    }
    else {
        $error = $cliente->getError();
        if ($error) {
            echo "<h2>Error</h2><pre>" . $error . "</pre>";
        }
        else {
            echo "<h2>Modificacion autorización abonado</h2><pre>";
            echo $result;
            echo "</pre>";
        }
    }
    
    $result = $cliente->call("getBajaAbonado", array("identificacion" => "787570K"));
      
    if ($cliente->fault) {
        echo "<h2>Fault</h2><pre>";
        print_r($result);
        echo "</pre>";
    }
    else {
        $error = $cliente->getError();
        if ($error) {
            echo "<h2>Error</h2><pre>" . $error . "</pre>";
        }
        else {
            echo "<h2>Baja autorización abonado</h2><pre>";
            echo $result;
            echo "</pre>";
        }
    }
    
    
    $result = $cliente->call("getRecargaSaldo", array("identificacion" => "72494410C"));
      
    if ($cliente->fault) {
        echo "<h2>Fault</h2><pre>";
        print_r($result);
        echo "</pre>";
    }
    else {
        $error = $cliente->getError();
        if ($error) {
            echo "<h2>Error</h2><pre>" . $error . "</pre>";
        }
        else {
            echo "<h2>Recarga Saldo Abonado</h2><pre>";
            echo $result;
            echo "</pre>";
        }
    }
    
    
    $result = $cliente->call("getConsultaSaldo", array("identificacion" => "78746570K"));
      
    if ($cliente->fault) {
        echo "<h2>Fault</h2><pre>";
        print_r($result);
        echo "</pre>";
    }
    else {
        $error = $cliente->getError();
        if ($error) {
            echo "<h2>Error</h2><pre>" . $error . "</pre>";
        }
        else {
            echo "<h2>Consulta Saldo Abonado</h2><pre>";
            echo $result;
            echo "</pre>";
        }
    }
    
    
    $result = $cliente->call("getDuplicadoTarjeta", array("identificacion" => "78746570K"));
      
    if ($cliente->fault) {
        echo "<h2>Fault</h2><pre>";
        print_r($result);
        echo "</pre>";
    }
    else {
        $error = $cliente->getError();
        if ($error) {
            echo "<h2>Error</h2><pre>" . $error . "</pre>";
        }
        else {
            echo "<h2>Duplicado Tarjeta</h2><pre>";
            echo $result;
            echo "</pre>";
        }
    }
?>
</body>
</html>
