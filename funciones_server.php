<?php
include ('funciones_bd.php');
include ('funciones_common.php');
header('Content-Type: text/html; charset=UTF-8');

function getAccesoTramites($dni,$clave) 
{
	$respuesta="KO";
	$db=abrir_bd();
	if($db)
	{
		$password=encrypt($clave);
		$resultado = selectInfoUsuario($dni,$password,$db);
		if($resultado==1)
		{
			$respuesta="OK";
		}
		cerrar_bd($db);
	}
	return $respuesta;
}

function getAltaAbonado($tipo_documento,$dni,$nombre,$apellido1,$apellido2,$fecha_nacimiento,$telefono,$email,$direccion,$municipio,$provincia,$codigo_postal,$abono_transporte,$saldo_inicial)
{
	$edad=CalculaEdad($fecha_nacimiento);						
								
	$ok=true;
	if($tipo_documento!=3)
	{
		$validar_dni=valida_nif_cif_nie($dni);
	}
	else
	{
		$validar_dni=4;
	}
	
	if(!empty($dni))
	{
		
		if($edad<16)
		{
			$ok=false;
		}							
		
		else if(($validar_dni!=1)&&($validar_dni!=2)&&($validar_dni!=3)&&($validar_dni!=4))
		{
			$ok=false;
		}
		else if(empty($edad))
		{
			$ok=false;
		}
		else if(empty($nombre))
		{
			$ok=false;
		}
		else if(empty($apellido1))
		{
			$ok=false;
		}
		else if(empty($apellido2))
		{
			$ok=false;
		}
		else if(empty($direccion))
		{
			$ok=false;
		}
		else if(empty($municipio))
		{
			$ok=false;
		}
		else if(empty($provincia))
		{
			$ok=false;
		}
		else if(empty($codigo_postal))
		{
			$ok=false;
		}
		else if(!is_numeric($codigo_postal))
		{
			$ok=false;
		}
		else if(empty($telefono))
		{
			$ok=false;
		}
		else if(strlen($telefono)<9)
		{
			$ok=false;
		}
		else if(!is_numeric($telefono))
		{
			$ok=false;
		}
		else if(empty($email))
		{
			$ok=false;
		}
		else if(!verificaremail($email))
		{
			$ok=false;
		}
		else if($abono_transporte!="NO")
		{
			//$tarjeta_valida=comprobarAT($abono_transporte);
			$tarjeta_valida=1;//Para que me funcione en boosterbikes
			if(strlen($abono_transporte)<22)
			{
				$ok=false;	
			}
			 else if($tarjeta_valida!=1)
			{
				$ok=false;
			}	
		}			
	}															
	else
	{
		$ok=false;
	}
		
	if($ok)
	{
		$respuesta="KO";
		$db=abrir_bd();
		if($db)
		{
			$resultado = can_subscribe($dni,$db);
			if($resultado=="t")
			{
				$respuesta="OK";
			}
			cerrar_bd($db);
		}
	}
	else
	{
		$respuesta="KO";	
	}
	
	return $respuesta;
 
}

function getModificarAbonado($dni,$nombre,$apellido1,$apellido2,$fecha_nacimiento,$telefono,$email,$direccion,$municipio,$provincia,$codigo_postal)
{
	$edad=CalculaEdad($fecha_nacimiento);						
								
	$ok=true;
	
	if($edad<16)
	{
		$ok=false;
	}							
	else if(empty($edad))
	{
		$ok=false;
	}
	else if(empty($nombre))
	{
		$ok=false;
	}
	else if(empty($apellido1))
	{
		$ok=false;
	}
	else if(empty($apellido2))
	{
		$ok=false;
	}
	else if(empty($direccion))
	{
		$ok=false;
	}
	else if(empty($municipio))
	{
		$ok=false;
	}
	else if(empty($provincia))
	{
		$ok=false;
	}
	else if(empty($codigo_postal))
	{
		$ok=false;
	}
	else if(!is_numeric($codigo_postal))
	{
		$ok=false;
	}
	else if(empty($telefono))
	{
		$ok=false;
	}
	else if(strlen($telefono)<9)
	{
		$ok=false;
	}
	else if(!is_numeric($telefono))
	{
		$ok=false;
	}
	else if(empty($email))
	{
		$ok=false;
	}
	else if(!verificaremail($email))
	{
		$ok=false;
	}		

		
    if($ok)
	{
		$respuesta="KO";
		$db=abrir_bd();
		if($db)
		{
			$resultado = updateUsuario($dni,$nombre,$apellido1,$apellido2,$fecha_nacimiento,$telefono,$email,$direccion,$municipio,$provincia,$codigo_postal,$db);
			if($resultado==1)
			{
				$respuesta="OK";
			}
			cerrar_bd($db);
		}
	}
	else
	{
		$respuesta="KO";	
	}
	
	return $respuesta;
}

function getBajaAbonado($dni) 
{
	$respuesta="KO";
	$db=abrir_bd();
	if($db)
	{
		$resultado = cancell_subscription($dni,$db);
		if($resultado==1)
		{
			$respuesta="OK";
		}
		cerrar_bd($db);
	}
	return $respuesta;
}

function getRecargaSaldo($dni)
{
	$respuesta="KO";
	$db=abrir_bd();
	if($db)
	{
		$resultado = check_active_subscrition($dni,$db);
		if($resultado==1)
		{
			$respuesta="OK";
		}
		cerrar_bd($db);
	}
	return $respuesta;
}

function getConsultaSaldo($dni)
{
	$respuesta="KO";
	$db=abrir_bd();
	if($db)
	{
		$resultado = check_amount_user($dni,$db);
		cerrar_bd($db);
	}
	return $resultado;
}

function getDuplicadoTarjeta($dni,$motivo)
{
	$respuesta="KO";
	$db=abrir_bd();
	if($db)
	{
		$resultado = check_active_subscrition($dni,$db);
		if($resultado==1)
		{
			$respuesta="OK";
		}
		/*$resultado = duplicate_card($dni,$motivo,$db);
		$resultado2 = split('/', $resultado);
		if($resultado2[0])
		{
			$respuesta=$resultado2[1];
		} */
		cerrar_bd($db);
	}
	return $respuesta;
}

  
?>
